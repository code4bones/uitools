include theos/makefiles/common.mk

TOOL_NAME = pbcopy pbpaste play post say
SUBPROJECTS = sbtools

pbcopy_FILES = pbcopy.m
pbcopy_FRAMEWORKS = UIKit

pbpaste_FILES = pbpaste.m
pbpaste_FRAMEWORKS = UIKit

play_FILES = play.m
play_FRAMEWORKS = AVFoundation

post_FILES = post.c
post_FRAMEWORKS = CoreFoundation

say_FILES = say.m
say_PRIVATE_FRAMEWORKS = VoiceServices

include $(THEOS_MAKE_PATH)/tool.mk
include $(THEOS_MAKE_PATH)/aggregate.mk
