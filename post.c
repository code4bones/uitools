#import <CoreFoundation/CoreFoundation.h>
#include <stdio.h>

int main(int argc,char** argv) {
  if(argc<2){
    fprintf(stderr,"Usage: %s <notification>\n",argv[0]);
    return 1;
  }
  CFStringRef name=CFStringCreateWithCString(NULL,argv[1],kCFStringEncodingUTF8);
  CFNotificationCenterPostNotification(CFNotificationCenterGetDarwinNotifyCenter(),name,NULL,NULL,TRUE);
  CFRelease(name);
  return 0;
}
