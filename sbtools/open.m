NSArray* SBSCopyPublicURLSchemes();
NSArray* SBSCopyApplicationDisplayIdentifiers(bool onlyActive,bool debuggable);
NSString* SBSCopyLocalizedApplicationNameForDisplayIdentifier(NSString* appID);
bool SBSOpenSensitiveURLAndUnlock(NSURL* URL,char flags);
int SBSLaunchApplicationForDebugging(NSString* appID,NSURL* URL,NSArray* args,NSDictionary* env,NSString* stdout,NSString* stderr,char flags);
bool SBSProcessIDForDisplayIdentifier(NSString* appID,pid_t *pid);
NSString* SBSApplicationLaunchingErrorString(int error);

int main(int argc,char** argv) {
  NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
  NSString* appID=nil,*outFile=nil,*errFile=nil;
  NSURL* destURL=nil;
  int opt;
  while((opt=getopt(argc,argv,"a:o:e:"))!=-1){
    if(!optarg){return 1;}
    NSString* arg=[NSString stringWithUTF8String:optarg];
    switch(opt){
      case 'a':appID=arg;break;
      case 'o':outFile=arg;break;
      case 'e':errFile=arg;break;
      default:fprintf(stderr,"Warning: Ignoring option -%c\n",opt);break;
    }
  }
  NSMutableArray* args=[NSMutableArray arrayWithCapacity:1];
  for (opt=optind;opt<argc;opt++){
    NSString* arg=[NSString stringWithUTF8String:argv[opt]];
    if(!destURL){destURL=[NSURL URLWithString:
     [arg stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];}
    else if(!appID){fprintf(stderr,"Warning: Ignoring argument [%s]\n",argv[opt]);}
    else {[args addObject:arg];}
  }
  if(appID){
    NSString* appName=[SBSCopyLocalizedApplicationNameForDisplayIdentifier(appID) autorelease];
    BOOL found=appName?YES:NO;
    NSMutableString* appList=[NSMutableString stringWithCapacity:1024];
    NSUInteger appCount=0;
    if(!found){
      for (NSString* ID in [[SBSCopyApplicationDisplayIdentifiers(false,false) autorelease]
       sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)]){
        appName=[SBSCopyLocalizedApplicationNameForDisplayIdentifier(ID) autorelease];
        if([appID caseInsensitiveCompare:appName]==NSOrderedSame){
          appID=ID;
          found=YES;
          break;
        }
        [appList appendFormat:@"  %@ (%@)\n",ID,appName];
        appCount++;
      }
    }
    printf("(%s)",[appID UTF8String]);
    BOOL merge=NO;
    if(outFile && (outFile=[[NSURL fileURLWithPath:outFile] path])){
      if((merge=!errFile)){printf(" &> %s",[errFile=outFile UTF8String]);}
      else {printf(" 1> %s",[outFile UTF8String]);}
    }
    if(!merge && errFile && (errFile=[[NSURL fileURLWithPath:errFile] path])){
      printf(" 2> %s",[errFile UTF8String]);
    }
    fputc('\n',stdout);
    int error=SBSLaunchApplicationForDebugging(appID,destURL,args,NULL,outFile,errFile,4);
    if(error){
      fprintf(stderr,"SBSLaunchApplicationForDebugging: %s\n",
       [SBSApplicationLaunchingErrorString(error) UTF8String]);
      if(!found){fprintf(stderr,"Recognized applications (%u):\n%s",appCount,[appList UTF8String]);}
      return error;
    }
    pid_t pid;
    while(!SBSProcessIDForDisplayIdentifier(appID,&pid)){usleep(100000);}
    printf(">> PID %d\n",(int)pid);
  }
  else if(destURL){
    if(outFile){fputs("Warning: Ignoring option -o\n",stderr);}
    if(errFile){fputs("Warning: Ignoring option -e\n",stderr);}
    NSArray* schemes=[SBSCopyPublicURLSchemes() autorelease];
    if(![schemes containsObject:[destURL scheme]]){
      fprintf(stderr,"Recognized URL schemes (%u):\n",[schemes count]);
      for (NSString* scheme in [schemes sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)]){
        printf("  %s\n",[scheme UTF8String]);
      }
    }
    SBSOpenSensitiveURLAndUnlock(destURL,1);
  }
  else {
    fprintf(stderr,"Usage: %s [-a appID] [-o stdout] [-e stderr] [URL] [arguments...]\n",argv[0]);
    return 1;
  }
  [pool drain];
  return 0;
}
