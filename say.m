#define OPT_RATE 1
#define OPT_VOLUME 2

static CFRunLoopRef $_mainRunLoop;

@interface VSSpeechSynthesizer : NSObject
@property(assign) id delegate;
@property(assign) float rate,volume;
-(BOOL)isSpeaking;
-(id)startSpeakingString:(NSString*)string;
-(id)startSpeakingString:(NSString*)string toURL:(NSURL*)URL;
@end

@interface SpeechSynthesizerDelegate : NSObject @end
@implementation SpeechSynthesizerDelegate
+(void)speechSynthesizer:(VSSpeechSynthesizer*)synth didFinishSpeaking:(BOOL)finished withError:(NSError*)error {
  if(!finished){NSLog(@"%@",error.description);}
  CFRunLoopStop($_mainRunLoop);
}
@end

int main(int argc,char** argv) {
  char options=0;
  char* outfn=NULL;
  float s_volume,s_rate;
  int opt;
  while((opt=getopt(argc,argv,"o:r:V:"))!=-1){
    if(!optarg){return 1;}
    if(opt=='o'){outfn=optarg;continue;}
    char ierr=(opt=='r' && (options|=OPT_RATE))?(sscanf(optarg,"%f",&s_rate)!=1):
     (opt=='V' && (options|=OPT_VOLUME))?(sscanf(optarg,"%f",&s_volume)!=1):2;
    if(ierr==2){fprintf(stderr,"Warning: Ignoring option -%c\n",opt);}
    else if(ierr){
      fprintf(stderr,"-%c: Invalid argument\n",opt);
      return 1;
    }
  }
  if(optind>=argc && isatty(fileno(stdin))){
    fprintf(stderr,"Usage: %s [-o output] [-r rate] [-V volume] [string] < [file]\n",argv[0]);
    return 1;
  }
  $_mainRunLoop=CFRunLoopGetCurrent();
  NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
  NSString* string=nil;
  if(optind!=argc-1 || strcmp(argv[optind],"-")!=0){
    NSMutableString* buffer=[NSMutableString string];
    for (opt=optind;opt<argc;opt++){[buffer appendFormat:@"%s ",argv[opt]];}
    if(buffer.length){string=buffer;}
  }
  if(!string){
    string=[[[NSString alloc] initWithData:[[NSFileHandle
     fileHandleWithStandardInput] readDataToEndOfFile]
     encoding:NSUTF8StringEncoding] autorelease];
  }
  VSSpeechSynthesizer* synth=[[VSSpeechSynthesizer alloc] init];
  synth.delegate=[SpeechSynthesizerDelegate class];
  if(options&OPT_RATE){synth.rate=s_rate;}
  if(options&OPT_VOLUME){synth.volume=s_volume;}
  if(outfn){
    [synth startSpeakingString:string toURL:[NSURL
     fileURLWithPath:[[NSFileManager defaultManager]
     stringWithFileSystemRepresentation:outfn length:strlen(outfn)]]];
  }
  else {[synth startSpeakingString:string];}
  if(synth.isSpeaking){CFRunLoopRun();}
  [synth release];
  [pool drain];
}
