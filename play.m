#import <AVFoundation/AVFoundation.h>

#define OPT_NLOOPS 1
#define OPT_GAIN 2
#define OPT_PAN 4

static CFRunLoopRef $_mainRunLoop;

@interface AudioPlayerDelegate : NSObject @end
@implementation AudioPlayerDelegate
+(void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer*)player error:(NSError*)error {
  NSLog(@"%@",error.description);
}
+(void)audioPlayerDidFinishPlaying:(AVAudioPlayer*)player successfully:(BOOL)success {
  CFRunLoopStop($_mainRunLoop);
}
@end

int main(int argc,char** argv) {
  char options=0;
  NSInteger p_nloops;
  float p_gain,p_pan;
  int opt;
  while((opt=getopt(argc,argv,"n:g:p:"))!=-1){
    if(!optarg){return 1;}
    char ierr=(opt=='n' && (options|=OPT_NLOOPS))?(sscanf(optarg,"%d",&p_nloops)!=1):
     (opt=='g' && (options|=OPT_GAIN))?(sscanf(optarg,"%f",&p_gain)!=1 || p_gain<0 || p_gain>1):
     (opt=='p' && (options|=OPT_PAN))?(sscanf(optarg,"%f",&p_pan)!=1 || p_pan<-1 || p_pan>1):2;
    if(ierr==2){fprintf(stderr,"Warning: Ignoring option -%c\n",opt);}
    else if(ierr){
      fprintf(stderr,"-%c: Invalid argument\n",opt);
      return 1;
    }
  }
  if(optind>=argc){
    fprintf(stderr,"Usage: %s [-n nloops] [-g gain(0..1)] [-p pan(-1..1)] <file>\n",argv[0]);
    return 1;
  }
  $_mainRunLoop=CFRunLoopGetCurrent();
  NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
  AVAudioSession* session=[AVAudioSession sharedInstance];
  NSError* error;
  if(![session setCategory:AVAudioSessionCategoryPlayback error:&error]){
    NSLog(@"AVAudioSession setCategory %@",error.description);
  }
  if(![session setActive:YES error:&error]){
    NSLog(@"AVAudioSession setActive %@",error.description);
  }
  for (opt=optind;opt<argc;opt++){
    char* infn=argv[opt];
    NSURL* URL=[NSURL fileURLWithPath:[[NSFileManager defaultManager]
     stringWithFileSystemRepresentation:infn length:strlen(infn)]];
    printf("Playing [%s]...\n",URL.path.UTF8String);
    AVAudioPlayer* player=[[AVAudioPlayer alloc] initWithContentsOfURL:URL error:&error];
    if(!player){
      NSLog(@"%@",error.description);
      continue;
    }
    player.delegate=(id)[AudioPlayerDelegate class];
    if(options&OPT_NLOOPS){player.numberOfLoops=p_nloops;}
    if(options&OPT_GAIN){player.volume=p_gain;}
    if(options&OPT_PAN){player.pan=p_pan;}
    [player play];
    CFRunLoopRun();
    [player stop];
    [player release];
  }
  [pool drain];
}
